﻿using Cryptocurrencies.BL.Helpers;
using Cryptocurrencies.BL.Services;
using Moq;
using NUnit.Framework;

namespace Cryptocurrencies.Tests.ServicesTests
{
    [TestFixture]
    public class CurrencyGeneratorTests
    {
        [Test]
        public void SetStartRates_validData_minValue()
        {
            //Arrange
            var randomProviderMock = new Mock<IRandomProvider>();
            var service = new CurrencyGenerator(randomProviderMock.Object);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[0].MinValue, service.ListOfCurrenciesBl[0].MaxValue)).Returns(service.ListOfCurrenciesBl[0].MinValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[1].MinValue, service.ListOfCurrenciesBl[1].MaxValue)).Returns(service.ListOfCurrenciesBl[1].MinValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[2].MinValue, service.ListOfCurrenciesBl[2].MaxValue)).Returns(service.ListOfCurrenciesBl[2].MinValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[3].MinValue, service.ListOfCurrenciesBl[3].MaxValue)).Returns(service.ListOfCurrenciesBl[3].MinValue);
            //Act
            service.SetStartRates();
            //Assert
            Assert.AreEqual(10000, service.ListOfCurrenciesBl[0].Value);
            Assert.AreEqual(2000, service.ListOfCurrenciesBl[1].Value);
            Assert.AreEqual(1800, service.ListOfCurrenciesBl[2].Value);
            Assert.AreEqual(100, service.ListOfCurrenciesBl[3].Value);
        }

        [Test]
        public void SetStartRates_validData_maxValue()
        {
            //Arrange
            var randomProviderMock = new Mock<IRandomProvider>();
            var service = new CurrencyGenerator(randomProviderMock.Object);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[0].MinValue, service.ListOfCurrenciesBl[0].MaxValue)).Returns(service.ListOfCurrenciesBl[0].MaxValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[1].MinValue, service.ListOfCurrenciesBl[1].MaxValue)).Returns(service.ListOfCurrenciesBl[1].MaxValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[2].MinValue, service.ListOfCurrenciesBl[2].MaxValue)).Returns(service.ListOfCurrenciesBl[2].MaxValue);
            randomProviderMock.Setup(x => x.GetRandom(service.ListOfCurrenciesBl[3].MinValue, service.ListOfCurrenciesBl[3].MaxValue)).Returns(service.ListOfCurrenciesBl[3].MaxValue);
            //Act
            service.SetStartRates();
            //Assert
            Assert.AreEqual(40000, service.ListOfCurrenciesBl[0].Value);
            Assert.AreEqual(10000, service.ListOfCurrenciesBl[1].Value);
            Assert.AreEqual(2400, service.ListOfCurrenciesBl[2].Value);
            Assert.AreEqual(400, service.ListOfCurrenciesBl[3].Value);
        }

        [Test]
        public void TimerElapsedCallback_validData_validResult()
        {
            //Arrange
            var randomProviderMock = new Mock<IRandomProvider>();
            var service = new CurrencyGenerator(randomProviderMock.Object);
            service.ListOfCurrenciesBl[0].Value = 20000;
            service.ListOfCurrenciesBl[1].Value = 4000;
            service.ListOfCurrenciesBl[2].Value = 2100;
            service.ListOfCurrenciesBl[3].Value = 150;
            randomProviderMock.Setup(x => x.GetRandom(18000, 22000)).Returns(21000);
            randomProviderMock.Setup(x => x.GetRandom(3600, 4400)).Returns(4100);
            randomProviderMock.Setup(x => x.GetRandom(1890, 2310)).Returns(2150);
            randomProviderMock.Setup(x => x.GetRandom(135, 165)).Returns(140);
            //Act
            service.TimerElapsedCallback(new object(), null);
            //Assert
            Assert.AreEqual(21000, service.ListOfCurrenciesBl[0].Value);
            Assert.AreEqual(4100, service.ListOfCurrenciesBl[1].Value);
            Assert.AreEqual(2150, service.ListOfCurrenciesBl[2].Value);
            Assert.AreEqual(140, service.ListOfCurrenciesBl[3].Value);
        }

        [Test]
        public void TimerElapsedCallback_validData_minResult()
        {
            //Arrange
            var randomProviderMock = new Mock<IRandomProvider>();
            var service = new CurrencyGenerator(randomProviderMock.Object);
            service.ListOfCurrenciesBl[0].Value = 10000;
            service.ListOfCurrenciesBl[1].Value = 2000;
            service.ListOfCurrenciesBl[2].Value = 1800;
            service.ListOfCurrenciesBl[3].Value = 100;
            randomProviderMock.Setup(x => x.GetRandom(10000, 11000)).Returns(10500);
            randomProviderMock.Setup(x => x.GetRandom(2000, 2200)).Returns(2100);
            randomProviderMock.Setup(x => x.GetRandom(1800, 1980)).Returns(1900);
            randomProviderMock.Setup(x => x.GetRandom(100, 110)).Returns(105);
            //Act
            service.TimerElapsedCallback(new object(), null);
            //Assert
            Assert.AreEqual(10500, service.ListOfCurrenciesBl[0].Value);
            Assert.AreEqual(2100, service.ListOfCurrenciesBl[1].Value);
            Assert.AreEqual(1900, service.ListOfCurrenciesBl[2].Value);
            Assert.AreEqual(105, service.ListOfCurrenciesBl[3].Value);
        }

        [Test]
        public void TimerElapsedCallback_validData_maxResult()
        {
            //Arrange
            var randomProviderMock = new Mock<IRandomProvider>();
            var service = new CurrencyGenerator(randomProviderMock.Object);
            service.ListOfCurrenciesBl[0].Value = 40000;
            service.ListOfCurrenciesBl[1].Value = 10000;
            service.ListOfCurrenciesBl[2].Value = 2400;
            service.ListOfCurrenciesBl[3].Value = 400;
            randomProviderMock.Setup(x => x.GetRandom(36000, 40000)).Returns(38000);
            randomProviderMock.Setup(x => x.GetRandom(9000, 10000)).Returns(9900);
            randomProviderMock.Setup(x => x.GetRandom(2160, 2400)).Returns(2300);
            randomProviderMock.Setup(x => x.GetRandom(360, 400)).Returns(380);
            //Act
            service.TimerElapsedCallback(new object(), null);
            //Assert
            Assert.AreEqual(38000, service.ListOfCurrenciesBl[0].Value);
            Assert.AreEqual(9900, service.ListOfCurrenciesBl[1].Value);
            Assert.AreEqual(2300, service.ListOfCurrenciesBl[2].Value);
            Assert.AreEqual(380, service.ListOfCurrenciesBl[3].Value);
        }
    }
}
