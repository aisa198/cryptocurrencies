﻿using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Mappers;
using Cryptocurrencies.DAL.Models;
using NUnit.Framework;

namespace Cryptocurrencies.Tests.MappingTests
{
    [TestFixture]
    public class UserMapperTests
    {
        [Test]
        public void MapUserToUserBl_null_null()
        {
            //Arrange
            var userMapper = new UserMapper();
            User user = null;
            //Act
            var userBl = userMapper.MapUserToUserBl(user);
            //Assert
            Assert.IsNull(userBl);
        }

        [Test]
        public void MapUserBlToUser_null_null()
        {
            //Arrange
            var userMapper = new UserMapper();
            UserBl userBl = null;
            //Act
            var user = userMapper.MapUserBlToUser(userBl);
            //Assert
            Assert.IsNull(user);
        }

        [Test]
        public void MapUserToUserBl_validData_validResult()
        {
            //Arrange
            var userMapper = new UserMapper();
            var user = new User
            {
                Id = 1,
                Login = "aisa198",
                Name = "Asia",
                Password = "pass"
            };
            //Act
            var userBl = userMapper.MapUserToUserBl(user);
            //Assert
            Assert.AreEqual(1, userBl.Id);
            Assert.AreEqual("aisa198", userBl.Login);
            Assert.AreEqual("Asia", userBl.Name);
            Assert.AreEqual("pass", userBl.Password);
        }

        [Test]
        public void MapUserBlToUser_validData_validResult()
        {
            //Arrange
            var userMapper = new UserMapper();
            var userBl = new UserBl
            {
                Id = 1,
                Login = "aisa198",
                Name = "Asia",
                Password = "pass"
            };
            //Act
            var user = userMapper.MapUserBlToUser(userBl);
            //Assert
            Assert.AreEqual(1, user.Id);
            Assert.AreEqual("aisa198", user.Login);
            Assert.AreEqual("Asia", user.Name);
            Assert.AreEqual("pass", user.Password);
        }
    }
}
