﻿using System.Collections.Generic;
using System.Linq;
using Cryptocurrencies.DAL.Data;
using Cryptocurrencies.DAL.Models;
using Cryptocurrencies.DAL.Repositories.Interfaces;

namespace Cryptocurrencies.DAL.Repositories
{
    public class UserRepository : IUserRepository
    {
        public List<User> GetAllUsers()
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Users
                    .ToList();
            }
        }

        public User GetUser(int userId)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Users.SingleOrDefault(x => x.Id == userId);
            }
        }

        public User AddUser(User user)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                var newUser = dbContext.Users.Add(user);
                dbContext.SaveChanges();
                return user;
            }
        }

        public User LogIn(string login)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Users.SingleOrDefault(x => x.Login == login);
            }
        }

        public bool CheckPassword(string login, string password)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                var foundUser = dbContext.Users.SingleOrDefault(x => x.Login == login);

                if (foundUser == null)
                {
                    return false;
                }

                return foundUser.Password == password;
            }
        }
    }
}
