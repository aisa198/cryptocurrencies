﻿using System.Collections.Generic;
using System.Linq;
using Cryptocurrencies.Comon.Enums;
using Cryptocurrencies.DAL.Mapping;
using Cryptocurrencies.DAL.Models;
using Cryptocurrencies.DAL.Repositories.Interfaces;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cryptocurrencies.DAL.Repositories
{
    public class TransactionsRepositoryAzure : BaseRepository, ITransactionsRepository
    {
        private readonly ITransactionsMapper _transactionsMapper;

        public TransactionsRepositoryAzure(string tableName, ITransactionsMapper transactionsMapper) : base("Transactions")
        {
            _transactionsMapper = transactionsMapper;
        }

        public Transaction AddTransaction(Transaction transaction)
        {
            var transactionAzure = _transactionsMapper.MapTransactionToTransactionAzure(transaction);
            var operation = TableOperation.Insert(transactionAzure);
            TableReference.Execute(operation);
            return transaction;
        }

        public List<Transaction> GetTransactionsForUser(int userId)
        {
            var transactions = new List<Transaction>();
            var transactionsAzure =  TableReference.CreateQuery<TransactionAzure>().Where(x => x.UserId == userId).ToList();

            foreach (var transactionAzure in transactionsAzure)
            {
                transactions.Add(_transactionsMapper.MapTransactionAzureToTransaction(transactionAzure));
            }

            return transactions;
        }

        public double? SumAllTransactions(User user)
        {
            var transactions = TableReference.CreateQuery<TransactionAzure>().Where(x => x.UserId == user.Id);
            double sum = 0;
            foreach (var transaction in transactions)
            {
                sum += transaction.Value;
            }

            return sum;
        }

        public double? SumTransactionsByCurrencyType(CurrencyType currencyToSell, User user)
        {
            var transactions = TableReference.CreateQuery<TransactionAzure>().Where(x => x.UserId == user.Id).ToList();
            var transactionsWithRightCurrency = transactions.Where(x => x.CurrencyType == currencyToSell);

            return transactionsWithRightCurrency.Sum(transaction => transaction.NumberOfCoins);
        }
    }
}
