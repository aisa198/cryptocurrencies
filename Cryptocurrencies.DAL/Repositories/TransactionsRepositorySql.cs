﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Cryptocurrencies.Comon.Enums;
using Cryptocurrencies.DAL.Data;
using Cryptocurrencies.DAL.Mapping;
using Cryptocurrencies.DAL.Models;
using Cryptocurrencies.DAL.Repositories.Interfaces;

namespace Cryptocurrencies.DAL.Repositories
{
    public class TransactionsRepositorySql : ITransactionsRepository
    {
        private readonly ITransactionsMapper _transactionsMapper;

        public TransactionsRepositorySql(ITransactionsMapper transactionsMapper)
        {
            _transactionsMapper = transactionsMapper;
        }

        public Transaction AddTransaction(Transaction transaction)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                if (transaction.User != null && !dbContext.Users.Local.Contains(transaction.User))
                {
                    dbContext.Users.Attach(transaction.User);
                    dbContext.Entry(transaction.User).State = EntityState.Modified;
                }
                var newTransaction = dbContext.Transactions.Add(transaction);
                dbContext.SaveChanges();
                return newTransaction;
            }
        }

        public double? SumAllTransactions(User user)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Transactions
                    .Where(x => x.User.Id == user.Id)
                    .Sum(x => (double?)x.Value);
            }
        }

        public double? SumTransactionsByCurrencyType(CurrencyType currencyToSell, User user)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Transactions
                    .Where(x => x.CurrencyType == currencyToSell && x.User.Id == user.Id)
                    .Sum(x => x.NumberOfCoins);
            }
        }

        public List<Transaction> GetTransactionsForUser(int userId)
        {
            using (var dbContext = new CryptocurrenciesDbContext())
            {
                return dbContext.Transactions.Where(x => x.User.Id == userId).Include(x => x.User).OrderBy(x => x.Date).ToList();
            }
        }
    }
}
