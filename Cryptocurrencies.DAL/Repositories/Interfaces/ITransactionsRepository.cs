﻿using System.Collections.Generic;
using Cryptocurrencies.Comon.Enums;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.DAL.Repositories.Interfaces
{
    public interface ITransactionsRepository
    {
        Transaction AddTransaction(Transaction transaction);
        double? SumAllTransactions(User user);
        double? SumTransactionsByCurrencyType(CurrencyType currencyToSell, User user);
        List<Transaction> GetTransactionsForUser(int userId);
    }
}