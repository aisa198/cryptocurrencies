﻿using System.Collections.Generic;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.DAL.Repositories.Interfaces
{
    public interface IUserRepository
    {
        List<User> GetAllUsers();
        User GetUser(int userId);
        User AddUser(User user);
        User LogIn(string login);
        bool CheckPassword(string login, string password);
    }
}