﻿using System.Configuration;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cryptocurrencies.DAL.Repositories
{
    public class BaseRepository
    {
        protected readonly CloudTable TableReference;

        public BaseRepository(string tableName)
        {
            var cloudStorageAccount = CloudStorageAccount.Parse(ConfigurationManager.AppSettings["azureConnectionString"]);
            var tableClient = cloudStorageAccount.CreateCloudTableClient();
            TableReference = tableClient.GetTableReference(tableName);
            TableReference.CreateIfNotExists();
        }
    }
}
