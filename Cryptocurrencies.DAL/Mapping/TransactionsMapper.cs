﻿using Cryptocurrencies.DAL.Models;
using Cryptocurrencies.DAL.Repositories.Interfaces;

namespace Cryptocurrencies.DAL.Mapping
{
    public class TransactionsMapper : ITransactionsMapper
    {
        private readonly IUserRepository _userRepository;

        public TransactionsMapper(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public Transaction MapTransactionAzureToTransaction(TransactionAzure transaction)
        {
            if (transaction == null)
            {
                return null;
            }

            var transactionSql = new Transaction
            {
                Id = transaction.Id,
                CurrencyType = transaction.CurrencyType,
                NumberOfCoins = transaction.NumberOfCoins,
                Value = transaction.Value,
                Date = transaction.Date,
                Type = transaction.Type
            };
            transactionSql.User = transactionSql.User = _userRepository.GetUser(transaction.UserId);

            return transactionSql;
        }

        public TransactionAzure MapTransactionToTransactionAzure(Transaction transaction)
        {
            if (transaction == null)
            {
                return null;
            }

            var transactionAzure = new TransactionAzure
            {
                Id = transaction.Id,
                UserId = transaction.User.Id,
                CurrencyType = transaction.CurrencyType,
                NumberOfCoins = transaction.NumberOfCoins,
                Value = transaction.Value,
                Date = transaction.Date,
                Type = transaction.Type
            };

            return transactionAzure;
        }
    }
}
