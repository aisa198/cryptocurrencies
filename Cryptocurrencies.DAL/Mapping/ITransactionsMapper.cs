﻿using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.DAL.Mapping
{
    public interface ITransactionsMapper
    {
        Transaction MapTransactionAzureToTransaction(TransactionAzure transaction);
        TransactionAzure MapTransactionToTransactionAzure(Transaction transaction);
    }
}