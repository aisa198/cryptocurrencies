﻿using System.Collections.Generic;

namespace Cryptocurrencies.DAL.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<TransactionAzure> Transactions { get; set; }
    }
}
