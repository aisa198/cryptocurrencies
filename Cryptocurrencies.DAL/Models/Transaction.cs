﻿using System;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.DAL.Models
{
    public class Transaction
    {
        public Guid Id { get; set; }
        public User User { get; set; }
        public CurrencyType? CurrencyType { get; set; }
        public TransactionType Type { get; set; }
        public double? NumberOfCoins { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
    }
}
