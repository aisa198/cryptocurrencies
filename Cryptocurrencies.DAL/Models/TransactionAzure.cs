﻿using System;
using Cryptocurrencies.Comon.Enums;
using Microsoft.WindowsAzure.Storage.Table;

namespace Cryptocurrencies.DAL.Models
{
    public class TransactionAzure : TableEntity
    {
        public Guid Id
        {
            get => Guid.Parse(RowKey);
            set => RowKey = value.ToString("D");
        }

        public int UserId
        {
            get => int.Parse(PartitionKey);
            set => PartitionKey = value.ToString();
        }

        public CurrencyType? CurrencyType { get; set; }
        public int CurrencyTypeInt
        {
            get => CurrencyTypeInt = (CurrencyType == null) ? -1 : (int)CurrencyType;
            set => CurrencyType = (CurrencyType)value;
        }

        public TransactionType Type { get; set; }
        public int TransationTypeInt
        {
            get => (int)Type;
            set => Type = Enum.IsDefined(typeof(TransactionType), value) ? (TransactionType)value : throw new Exception("No such enum");
        }

        public double? NumberOfCoins { get; set; }
        public double Value { get; set; }
        public DateTime Date { get; set; }
    }
}
