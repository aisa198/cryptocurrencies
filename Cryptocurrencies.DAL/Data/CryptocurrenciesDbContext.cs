﻿using System.Configuration;
using System.Data.Entity;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.DAL.Data
{
    internal class CryptocurrenciesDbContext : DbContext
    {
        public CryptocurrenciesDbContext() : base(GetConnectionString())
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<Transaction> Transactions { get; set; }

        private static string GetConnectionString() => ConfigurationManager.ConnectionStrings["CryptocurrenciesDb"].ConnectionString;
    }
}
