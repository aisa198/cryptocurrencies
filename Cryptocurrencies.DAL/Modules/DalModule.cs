﻿using Cryptocurrencies.DAL.Mapping;
using Cryptocurrencies.DAL.Repositories;
using Cryptocurrencies.DAL.Repositories.Interfaces;
using Ninject.Modules;

namespace Cryptocurrencies.DAL.Modules
{
    public class DalModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUserRepository>().To<UserRepository>();
            Bind<ITransactionsMapper>().To<TransactionsMapper>();
            Bind<ITransactionsRepository>().To<TransactionsRepositorySql>();
            //Bind<ITransactionsRepository>().To<TransactionsRepositoryAzure>().WithConstructorArgument("tableName");
        }
    }
}
