﻿namespace Cryptocurrencies.Comon.Enums
{
    public enum CurrencyType
    {
        BTC,
        BCC,
        ETH,
        LTC
    }
}
