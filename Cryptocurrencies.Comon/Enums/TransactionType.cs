﻿namespace Cryptocurrencies.Comon.Enums
{
    public enum TransactionType
    {
        Income,
        Withdraw,
        Purchase,
        Sale
    }
}
