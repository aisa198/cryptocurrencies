﻿using Cryptocurrencies.CLI.IoHelpers;
using Cryptocurrencies.CLI.MenuOptions;
using Cryptocurrencies.CLI.Services;

namespace Cryptocurrencies.CLI
{
    internal class ProgramLoop
    {
        private readonly IMenu _menu;
        private readonly IUserManagementsService _userManagementsService;
        private readonly IIoHelper _helper;

        public ProgramLoop(IMenu menu, IUserManagementsService userManagementsService, IIoHelper helper)
        {
            _menu = menu;
            _userManagementsService = userManagementsService;
            _helper = helper;
            AddMainMenuCommands();
        }

        private void AddMainMenuCommands()
        {
            _menu.AddOption(new Option("Register", _userManagementsService.AddUser));
            _menu.AddOption(new Option("Log in", _userManagementsService.LogIn));
            _menu.ExitCommand = "Exit";
        }

        public void Run()
        {
            while (!_menu.Exit)
            {
                _menu.PrintMenuOptions();
                var command = _helper.GetData("Type commad: ");
                _menu.InvokeCommand(command);
            }
        }
    }
}
