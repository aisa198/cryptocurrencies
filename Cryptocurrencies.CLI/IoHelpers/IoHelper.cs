﻿using System;

namespace Cryptocurrencies.CLI.IoHelpers
{
    internal class IoHelper : IIoHelper
    {
        public void WriteMessage(string message)
        {
            Console.Write(message);
        }

        public void WriteMessageLine(string message)
        {
            Console.WriteLine(message);
        }

        public string GetData(string message)
        {
            WriteMessage(message);
            var input = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(input))
            {
                input = GetData("Provide data: ");
            }
            return input;
        }

        public double GetNumber(string messageToUser)
        {
            var input = GetData(messageToUser);
            double result;
            while (!double.TryParse(input, out result) || result < 0)
            {
                input = GetData($"Provide valid data (number above 0): ");
            }
            return result;
        }
    }
}
