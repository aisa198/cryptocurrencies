﻿namespace Cryptocurrencies.CLI.IoHelpers
{
    internal interface IIoHelper
    {
        string GetData(string message);
        void WriteMessage(string message);
        void WriteMessageLine(string message);
        double GetNumber(string messageToUser);
    }
}