﻿using System;
using Cryptocurrencies.CLI.Modules;
using Ninject;

namespace Cryptocurrencies.CLI
{
    internal class Program
    {
        private static void Main()
        {
            Console.BackgroundColor = ConsoleColor.DarkCyan;
            Console.Clear();
            Console.ForegroundColor = ConsoleColor.Black;
            var kernel = new StandardKernel();
            kernel.Load(new[] { new CliModule() });
            var programLoop = kernel.Get<ProgramLoop>();
            programLoop.Run();
        }
    }
}
