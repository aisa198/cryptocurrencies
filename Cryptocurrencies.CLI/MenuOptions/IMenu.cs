﻿namespace Cryptocurrencies.CLI.MenuOptions
{
    internal interface IMenu
    {
        bool Exit { get; set; }
        string ExitCommand { get; set; }
        void AddOption(Option option);
        void InvokeCommand(string command);
        void PrintMenuOptions();
    }
}