﻿using System;

namespace Cryptocurrencies.CLI.MenuOptions
{
    internal class Option
    {
        public string Command { get; }
        public Action Callback { get; }

        public Option(string command, Action callback)
        {
            Command = command;
            Callback = callback;
        }
    }
}
