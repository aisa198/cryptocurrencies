﻿using System.Collections.Generic;
using System.Linq;
using Cryptocurrencies.CLI.IoHelpers;

namespace Cryptocurrencies.CLI.MenuOptions
{
    internal class Menu : IMenu
    {
        private readonly List<Option> _options = new List<Option>();
        private readonly IIoHelper _helper;
        public bool Exit { get; set; }
        public string ExitCommand { get; set; }

        public Menu(IIoHelper helper)
        {
            _helper = helper;
        }

        public void AddOption(Option option)
        {
            _options.Add(option);
        }

        public void PrintMenuOptions()
        {
            _helper.WriteMessageLine("Menu options:");
            foreach (var option in _options)
            {
                _helper.WriteMessageLine($"- {option.Command}");
            }

            if (!string.IsNullOrWhiteSpace((ExitCommand)))
                _helper.WriteMessageLine($"- {ExitCommand}");
        }

        public void InvokeCommand(string command)
        {
            command = command.ToLower();
            if (command == ExitCommand.ToLower())
            {
                Exit = true;
                return;
            }

            var menuOption = _options.SingleOrDefault(o => o.Command.ToLower() == command);

            if (menuOption == null)
            {
                _helper.WriteMessageLine($"Command {command} undefined.");
            }
            else
            {
                menuOption.Callback();
            }
        }
    }
}
