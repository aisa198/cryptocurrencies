﻿using System;
using System.Linq;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Services;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.BL.Services.Interfaces;
using Cryptocurrencies.CLI.IoHelpers;
using Cryptocurrencies.CLI.MenuOptions;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.CLI.Services
{
    internal class UserManagementsService : IUserManagementsService
    {
        private readonly IUserService _userService;
        private readonly IIoHelper _helper;
        private readonly IMenu _menu;
        private readonly ICurrencyGenerator _currencyGenerator;
        private readonly ICurrencyProvider _currencyProvider;
        private readonly ITransactionsService _trasactionsService;
        private UserBl _activeUser;


        public UserManagementsService(IUserService userService, IIoHelper helper, IMenu menu, ITransactionsService transactionsService, ICurrencyGenerator currencyGenerator, ICurrencyProvider currencyProvider)
        {
            _userService = userService;
            _helper = helper;
            _menu = menu;
            _trasactionsService = transactionsService;
            _currencyGenerator = currencyGenerator;
            _currencyProvider = currencyProvider;
            _currencyGenerator.StartGenerate();
            AddMenuCommands();
        }

        public void AddUser()
        {
            var login = _helper.GetData("Provide login: ").ToLower();
            var listOfUsers = _userService.GetAllUsers();
            while (listOfUsers.Any(x => x.Login == login))
            {
                _helper.WriteMessageLine("Username already exists.");
                login = _helper.GetData("Provide another login: ");
            }
            var userBl = new UserBl
            {
                Login = login,
                Name = _helper.GetData("Provide name: "),
                Password = _helper.GetData("Provide password: ")
            };
            _userService.AddUser(userBl);
        }

        public void LogIn()
        {
            var login = _helper.GetData("Provide login: ");
            var password = _helper.GetData("Provide password: ");
            var userBl = _userService.LogIn(login);
            if (userBl == null)
            {
                _helper.WriteMessageLine("Invalid login.");
            }
            else if (userBl.Password == password)
            {
                _activeUser = userBl;
                _helper.WriteMessageLine($"Hello {_activeUser.Name}! ");
                _menu.Exit = false;
                while (!_menu.Exit)
                {
                    ShowMenu();
                }
                _activeUser = null;
            }
            else
            {
                _helper.WriteMessageLine("Invalid password.");
            }
        }

        private void ShowMenu()
        {
            _menu.Exit = false;
            _menu.PrintMenuOptions();
            var command = _helper.GetData("Type commad: ");
            _menu.InvokeCommand(command);
        }

        private void AddMenuCommands()
        {
            //_menu.AddOption(new Option("Show cryptocurrencies rates (from generator)", ShowGeneratedRates));
            _menu.AddOption(new Option("Show cryptocurrencies rates", ShowRatesFromBitbay));
            _menu.AddOption(new Option("Pay into your account", RegisterIncome));
            _menu.AddOption(new Option("Withdraw from your account", RegisterWithdraw));
            _menu.AddOption(new Option("Buy cryptocurrency", RegisterPurchase));
            _menu.AddOption(new Option("Sell cryptocurrency", RegisterSale));
            _menu.AddOption(new Option("Show transactions", ShowTransactions));
            _menu.AddOption(new Option("Export transactions to file", ExportTransactions));
            _menu.ExitCommand = "Log out";
        }

        private void ExportTransactions()
        {
            try
            {
                var filePath = _helper.GetData("Provide file path: ");
                _trasactionsService.ExportTransactions(filePath, _activeUser.Id);
            }
            catch (ArgumentException filePath)
            {
                _helper.WriteMessageLine($"Incorrect file path: {filePath}");
            }
            catch (Exception e)
            {
                _helper.WriteMessageLine($"Some error occured: {e.Message}");
            }
        }

        private void ShowTransactions()
        {
            var transactions = _trasactionsService.GetTransactions(_activeUser.Id);

            if (transactions.Count != 0)
            {
                _helper.WriteMessageLine("Your transactions:");
                foreach (var transaction in transactions)
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    switch (transaction.Type)
                    {
                        case TransactionType.Purchase:
                            _helper.WriteMessageLine($"{transaction.Date} ({transaction.Type}) {transaction.NumberOfCoins} {transaction.CurrencyType} for {-transaction.Value} PLN");
                            break;
                        case TransactionType.Sale:
                            _helper.WriteMessageLine($"{transaction.Date} ({transaction.Type}) {-transaction.NumberOfCoins} {transaction.CurrencyType} for {transaction.Value} PLN");
                            break;
                        default:
                            Console.ForegroundColor = ConsoleColor.Black;
                            _helper.WriteMessageLine($"{transaction.Date} ({transaction.Type}) {transaction.Value}");
                            break;
                    }
                }
                Console.ForegroundColor = ConsoleColor.Black;
            }
            else
            {
                _helper.WriteMessageLine("No transaction to show");
            }

        }

        private void RegisterSale()
        {
            CurrencyType currencyToSell;
            switch (_helper.GetData("Which  currency do you want to sell (BTC, BCC, ETH, LTC)? ").ToUpper())
            {
                case "BTC":
                    currencyToSell = CurrencyType.BTC;
                    break;
                case "BCC":
                    currencyToSell = CurrencyType.BCC;
                    break;
                case "ETH":
                    currencyToSell = CurrencyType.ETH;
                    break;
                case "LTC":
                    currencyToSell = CurrencyType.LTC;
                    break;
                default:
                    _helper.WriteMessageLine("Currency not found");
                    return;
            }
            var amount = _helper.GetNumber($"Provide amount of {currencyToSell} to exchange: ");
            if (_trasactionsService.RegisterSale(amount, _activeUser.Id, currencyToSell) == null)
            {
                _helper.WriteMessageLine("Lack of funds");
            }
        }

        private void RegisterPurchase()
        {
            CurrencyType currencyToBuy;
            switch (_helper.GetData("Which  currency do you want to buy (BTC, BCC, ETH, LTC)? ").ToUpper())
            {
                case "BTC":
                    currencyToBuy = CurrencyType.BTC;
                    break;
                case "BCC":
                    currencyToBuy = CurrencyType.BCC;
                    break;
                case "ETH":
                    currencyToBuy = CurrencyType.ETH;
                    break;
                case "LTC":
                    currencyToBuy = CurrencyType.LTC;
                    break;
                default:
                    _helper.WriteMessageLine("Currency not found");
                    return;
            }
            var amount = _helper.GetNumber("Provide amount of PLN to exchange: ");
            if (_trasactionsService.RegisterPurchase(amount, _activeUser.Id, currencyToBuy) == null)
            {
                _helper.WriteMessageLine("Lack of funds");
            }
        }

        private void RegisterWithdraw()
        {
            var amount = _helper.GetNumber("Provide amount of withdraw: ");
            if (_trasactionsService.RegisterWithdraw(amount, _activeUser.Id) == null)
            {
                _helper.WriteMessageLine("Lack of funds");
            }
        }

        private void RegisterIncome()
        {
            var amount = _helper.GetNumber("Provide amount of income: ");
            _trasactionsService.RegisterIncome(amount, _activeUser.Id);
        }

        private void ShowRatesFromBitbay()
        {
            ShowEvent();
            _currencyProvider.RatesGenerated += ShowEvent;
            ConsoleKey key;

            do
            {
                key = Console.ReadKey(true).Key;
            } while (key != ConsoleKey.Escape);
            _currencyProvider.RatesGenerated -= ShowEvent;
            Console.CursorVisible = true;

            var top = Console.CursorTop;
            Console.SetCursorPosition(0, top + 6);
            ShowMenu();
        }

        private void ShowEvent(object sender, GeneratedEventArgs e)
        {
            Console.CursorVisible = false;
            var top = Console.CursorTop;
            _helper.WriteMessageLine("The actual cryptocurrencies rates:");
            foreach (var currency in e.ListOfGeneratedCurrenciesBl)
            {
                _helper.WriteMessageLine($"{currency.Name} - {currency.Value}");
            }
            _helper.WriteMessageLine("(To exit press ESC...)");
            Console.SetCursorPosition(0, top);
        }

        private void ShowEvent()
        {
            Console.CursorVisible = false;
            var top = Console.CursorTop;
            _helper.WriteMessageLine("The actual cryptocurrencies rates:");
            foreach (var currency in _currencyProvider.ListOfCurrenciesBl)
            {
                _helper.WriteMessageLine($"{currency.Name} - {currency.Value}");
            }
            _helper.WriteMessageLine("(To exit press ESC...)");
            Console.SetCursorPosition(0, top);
        }

        private void ShowGeneratedRates()
        {
            _currencyGenerator.RatesGenerated += Show;
            ConsoleKey key;

            do
            {
                key = Console.ReadKey(true).Key;
            } while (key != ConsoleKey.Escape);
            _currencyGenerator.RatesGenerated -= Show;
            Console.CursorVisible = true;

            var top = Console.CursorTop;
            Console.SetCursorPosition(0, top + 6);
            ShowMenu();
        }

        private void Show(object sender, GeneratedEventArgs e)
        {
            Console.CursorVisible = false;
            var top = Console.CursorTop;
            _helper.WriteMessageLine("The actual cryptocurrencies rates:");
            foreach (var currency in e.ListOfGeneratedCurrenciesBl)
            {
                _helper.WriteMessageLine($"{currency.Name} - {currency.Value}");
            }
            _helper.WriteMessageLine("(To exit press ESC...)");
            Console.SetCursorPosition(0, top);
        }
    }
}
