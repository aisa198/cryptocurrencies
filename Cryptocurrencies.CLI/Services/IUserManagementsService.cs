﻿namespace Cryptocurrencies.CLI.Services
{
    internal interface IUserManagementsService
    {
        void AddUser();
        void LogIn();
    }
}