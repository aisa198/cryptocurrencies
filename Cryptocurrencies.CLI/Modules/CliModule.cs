﻿using Cryptocurrencies.BL.Modules;
using Cryptocurrencies.CLI.IoHelpers;
using Cryptocurrencies.CLI.MenuOptions;
using Cryptocurrencies.CLI.Services;
using Ninject.Modules;

namespace Cryptocurrencies.CLI.Modules
{
    public class CliModule : NinjectModule
    {
        public override void Load()
        {
            var kernel = Kernel;
            kernel?.Load(new[] {new BlModule()});
            Bind<IIoHelper>().To<IoHelper>();
            Bind<IMenu>().To<Menu>();
            Bind<IUserManagementsService>().To<UserManagementsService>();
        }
    }
}
