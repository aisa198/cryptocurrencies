﻿using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.BL.Mappers
{
    public interface ITransactionMapper
    {
        Transaction MapTransactionBlToTransaction(TransactionBl transactionBl);
        TransactionBl MapTransactionToTransactionBl(Transaction transaction);
    }
}