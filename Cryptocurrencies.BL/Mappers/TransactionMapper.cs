﻿using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.BL.Mappers
{
    public class TransactionMapper : ITransactionMapper
    {
        public Transaction MapTransactionBlToTransaction(TransactionBl transactionBl)
        {
            if (transactionBl == null)
            {
                return null;
            }

            var transaction = new Transaction
            {
                Id = transactionBl.Id,
                CurrencyType = transactionBl.CurrencyType,
                NumberOfCoins = transactionBl.NumberOfCoins,
                Value = transactionBl.Value,
                Date = transactionBl.Date,
                Type = transactionBl.Type
            };

            return transaction;
        }

        public TransactionBl MapTransactionToTransactionBl(Transaction transaction)
        {
            if (transaction == null)
            {
                return null;
            }

            var transactionBl = new TransactionBl
            {
                Id = transaction.Id,
                CurrencyType = transaction.CurrencyType,
                NumberOfCoins = transaction.NumberOfCoins,
                Value = transaction.Value,
                Date = transaction.Date,
                Type = transaction.Type
            };

            return transactionBl;
        }
    }
}
