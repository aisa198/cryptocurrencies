﻿using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.BL.Mappers
{
    public interface IUserMapper
    {
        User MapUserBlToUser(UserBl userBl);
        UserBl MapUserToUserBl(User user);
    }
}