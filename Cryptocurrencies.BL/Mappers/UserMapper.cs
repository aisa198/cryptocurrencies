﻿using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.BL.Mappers
{
    public class UserMapper : IUserMapper
    {
        public User MapUserBlToUser(UserBl userBl)
        {
            if (userBl == null)
            {
                return null;
            }

            var user = new User
            {
                Id = userBl.Id,
                Name = userBl.Name,
                Login = userBl.Login,
                Password = userBl.Password
            };

            return user;
        }

        public UserBl MapUserToUserBl(User user)
        {
            if (user == null)
            {
                return null;
            }

            var userBl = new UserBl
            {
                Id = user.Id,
                Name = user.Name,
                Login = user.Login,
                Password = user.Password
            };

            return userBl;
        }
    }
}
