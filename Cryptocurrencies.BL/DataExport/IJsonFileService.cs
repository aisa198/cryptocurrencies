﻿using Cryptocurrencies.DAL.Models;

namespace Cryptocurrencies.BL.DataExport
{
    public interface IJsonFileService
    {
        void Export(string fileName, Transaction[] transactions);
    }
}