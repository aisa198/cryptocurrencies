﻿using System.IO;
using System.Linq;
using Cryptocurrencies.DAL.Models;
using Newtonsoft.Json;

namespace Cryptocurrencies.BL.DataExport
{
    public class JsonFileService : IJsonFileService
    {
        public void Export(string filePath, Transaction[] transactions)
        {
            var transactionsQuery = from transaction in transactions
                                    select new
                                    {
                                        transaction.Id,
                                        UserId = transaction.User.Id,
                                        transaction.CurrencyType,
                                        transaction.NumberOfCoins,
                                        transaction.Date,
                                        transaction.Value,
                                        transaction.Type
                                    };

            var output = JsonConvert.SerializeObject(transactionsQuery, Formatting.Indented);
            File.AppendAllText(filePath, output);
        }
    }
}
