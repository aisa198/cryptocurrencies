﻿using System.Collections.Generic;

namespace Cryptocurrencies.BL.BusinessModels
{
    public class UserBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<TransactionBl> Transactions { get; set; }
    }
}
