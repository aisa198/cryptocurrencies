﻿using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.BL.BusinessModels
{
    public class CurrencyBl
    {
        public int Id { get; set; }
        public CurrencyType Name { get; set; }
        public double Value { get; set; }
        public int MinValue { get; set; }
        public int MaxValue { get; set; }
    }
}
