﻿using System;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.BL.BusinessModels
{
    public class TransactionBl
    {
        public Guid Id { get; set; }
        public UserBl UserBl { get; set; }
        public DateTime Date { get; set; }
        public CurrencyType? CurrencyType { get; set; }
        public TransactionType Type { get; set; }
        public double? NumberOfCoins { get; set; }
        public double Value { get; set; }
    }
}
