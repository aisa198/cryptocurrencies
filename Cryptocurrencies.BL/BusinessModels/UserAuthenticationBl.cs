﻿namespace Cryptocurrencies.BL.BusinessModels
{
    public class UserAuthenticationBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsAuthenticated { get; set; }
    }
}
