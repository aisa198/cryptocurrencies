﻿namespace Cryptocurrencies.BL.BusinessModels
{
    public class AccountBalanceBl
    {
        public double NumberOfPln { get; set; }
        public double NumberOfBtc { get; set; }
        public double NumberOfBcc { get; set; }
        public double NumberOfEth { get; set; }
        public double NumberOfLtc { get; set; }
    }
}
