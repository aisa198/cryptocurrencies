﻿using System;

namespace Cryptocurrencies.BL.Helpers
{
    public class RandomProvider : IRandomProvider
    {
        private readonly Random _random = new Random();

        public int GetRandom(int min, int max)
        {
            var value = _random.Next(min, max + 1);
            return value;
        }
    }
}
