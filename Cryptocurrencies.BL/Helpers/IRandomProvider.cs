﻿namespace Cryptocurrencies.BL.Helpers
{
    public interface IRandomProvider
    {
        int GetRandom(int min, int max);
    }
}