﻿using System;
using System.Collections.Generic;
using System.Timers;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Helpers;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.BL.Services
{
    public class CurrencyGenerator : ICurrencyGenerator
    {
        public List<CurrencyBl> ListOfCurrenciesBl { get; }
        private readonly IRandomProvider _random;
        public delegate void GeneratedHandler(object sender, GeneratedEventArgs e);
        public event GeneratedHandler RatesGenerated;
        
        public CurrencyGenerator(IRandomProvider randomProvider)
        {
            ListOfCurrenciesBl = new List<CurrencyBl>
            {
                new CurrencyBl
                {
                    Id = 1,
                    Name = CurrencyType.BTC,
                    MinValue = 10000,
                    MaxValue = 40000
                },
                new CurrencyBl
                {
                    Id = 2,
                    Name = CurrencyType.BCC,
                    MinValue = 2000,
                    MaxValue = 10000
                },
                new CurrencyBl
                {
                    Id = 3,
                    Name = CurrencyType.ETH,
                    MinValue = 1800,
                    MaxValue = 2400
                },
                new CurrencyBl
                {
                    Id = 4,
                    Name = CurrencyType.LTC,
                    MinValue = 100,
                    MaxValue = 400
                }
            };
            _random = randomProvider;
            SetStartRates();
        }

        public void SetStartRates()
        {
            foreach (var currency in ListOfCurrenciesBl)
            {
                currency.Value = _random.GetRandom(currency.MinValue, currency.MaxValue);
            }
        }

        public void StartGenerate()
        {
            var t = new Timer {Interval = 1000};
            t.Elapsed += TimerElapsedCallback;
            t.Start();
        }

        public void TimerElapsedCallback(object sender, ElapsedEventArgs e)
        {
            foreach (var currency in ListOfCurrenciesBl)
            {
                var minValue = Convert.ToInt32(currency.Value - (0.1 * currency.Value));
                var min = (minValue < currency.MinValue) ? currency.MinValue : minValue;
                var maxValue = Convert.ToInt32(currency.Value + (0.1 * currency.Value));
                var max = (maxValue > currency.MaxValue) ? currency.MaxValue : maxValue;
                currency.Value = _random.GetRandom(min, max);
            }
            var listOfArgs = new GeneratedEventArgs
            {
                ListOfGeneratedCurrenciesBl = ListOfCurrenciesBl
            };
            RatesGenerated?.Invoke(this, listOfArgs);
        }
    }
}
