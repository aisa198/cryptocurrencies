﻿using System.Collections.Generic;
using Cryptocurrencies.BL.BusinessModels;

namespace Cryptocurrencies.BL.Services.Interfaces
{
    public interface ICurrencyProvider
    {
        List<CurrencyBl> ListOfCurrenciesBl { get; }
        event CurrencyProvider.GeneratedHandler RatesGenerated;
        void SetValues();
        void StartGenerate();
    }
}