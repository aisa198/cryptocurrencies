﻿using System.Timers;

namespace Cryptocurrencies.BL.Services.interfaces
{
    public interface ICurrencyGenerator
    {
        event CurrencyGenerator.GeneratedHandler RatesGenerated;
        void SetStartRates();
        void StartGenerate();
        void TimerElapsedCallback(object sender, ElapsedEventArgs e);
    }
}