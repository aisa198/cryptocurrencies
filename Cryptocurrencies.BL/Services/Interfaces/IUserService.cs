﻿using System.Collections.Generic;
using Cryptocurrencies.BL.BusinessModels;

namespace Cryptocurrencies.BL.Services.interfaces
{
    public interface IUserService
    {
        List<UserBl> GetAllUsers();
        UserBl AddUser(UserBl userBl);
        UserBl LogIn(string login);
        UserAuthenticationBl CheckPassword(UserBl userBl);
    }
}