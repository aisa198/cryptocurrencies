﻿using System.Collections.Generic;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.BL.Services.interfaces
{
    public interface ITransactionsService
    {
        TransactionBl RegisterIncome(double amount, int userId);
        TransactionBl RegisterWithdraw(double amount, int userId);
        TransactionBl RegisterPurchase(double amount, int userId, CurrencyType currencyType);
        TransactionBl RegisterSale(double amount, int userId, CurrencyType currencyToSell);
        List<TransactionBl> GetTransactions(int userId);
        void ExportTransactions(string filePath, int userId);
        AccountBalanceBl GetAccountBalance(int userId);
    }
}