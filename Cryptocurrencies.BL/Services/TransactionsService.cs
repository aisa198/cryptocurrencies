﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.DataExport;
using Cryptocurrencies.BL.Mappers;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.BL.Services.Interfaces;
using Cryptocurrencies.Comon.Enums;
using Cryptocurrencies.DAL.Models;
using Cryptocurrencies.DAL.Repositories.Interfaces;

namespace Cryptocurrencies.BL.Services
{
    public class TransactionsesService : ITransactionsService
    {
        private readonly ITransactionMapper _transactionMapper;
        private readonly IUserRepository _userRepository;
        private readonly ITransactionsRepository _transactionRepository;
        private readonly ICurrencyProvider _currencyProvider;
        private readonly IJsonFileService _jsonFileService;

        public TransactionsesService(IUserRepository userRepository, ITransactionMapper transactionMapper, ITransactionsRepository transactionRepository, ICurrencyProvider currencyProvider, IJsonFileService jsonFileService)
        {
            _userRepository = userRepository;
            _transactionMapper = transactionMapper;
            _transactionRepository = transactionRepository;
            _currencyProvider = currencyProvider;
            _jsonFileService = jsonFileService;
        }

        public TransactionBl RegisterIncome(double amount, int userId)
        {
            if (amount <= 0) return null;
            var incomeBl = new TransactionBl
            {
                Id = Guid.NewGuid(),
                Value = amount,
                Type = TransactionType.Income,
                Date = DateTime.Now
            };

            var income = _transactionMapper.MapTransactionBlToTransaction(incomeBl);
            income.User = _userRepository.GetUser(userId);
            var newTransaction = _transactionRepository.AddTransaction(income);
            return _transactionMapper.MapTransactionToTransactionBl(newTransaction);
        }
        
        public TransactionBl RegisterWithdraw(double amount, int userId)
        {
            var user = _userRepository.GetUser(userId);
            var funds = _transactionRepository.SumAllTransactions(user);

            if (amount <= 0 || funds < amount || funds == null) return null;

            var withdrawBl = new TransactionBl
            {
                Id = Guid.NewGuid(),
                Value = -amount,
                Type = TransactionType.Withdraw,
                Date = DateTime.Now
            };

            var withdraw = _transactionMapper.MapTransactionBlToTransaction(withdrawBl);
            withdraw.User = user;
            var newTransaction  = _transactionRepository.AddTransaction(withdraw);
            return _transactionMapper.MapTransactionToTransactionBl(newTransaction);
        }

        public TransactionBl RegisterPurchase(double amount, int userId, CurrencyType currencyType)
        {
            var user = _userRepository.GetUser(userId);
            var funds = _transactionRepository.SumAllTransactions(user);
            if (amount <= 0 || funds < amount || funds == null) return null;

            var numberOfCoins = amount / _currencyProvider.ListOfCurrenciesBl.SingleOrDefault(x => x.Name == currencyType).Value;

            var purchaseBl = new TransactionBl
            {
                Id = Guid.NewGuid(),
                Value = -amount,
                Type = TransactionType.Purchase,
                CurrencyType = currencyType,
                NumberOfCoins = numberOfCoins,
                Date = DateTime.Now
            };

            var purchase = _transactionMapper.MapTransactionBlToTransaction(purchaseBl);
            purchase.User = user;
            var newTransaction = _transactionRepository.AddTransaction(purchase);
            return _transactionMapper.MapTransactionToTransactionBl(newTransaction);
        }

        public TransactionBl RegisterSale(double amount, int userId, CurrencyType currencyToSell)
        {
            var user = _userRepository.GetUser(userId);
            var funds = _transactionRepository.SumTransactionsByCurrencyType(currencyToSell, user);
            if (amount <= 0 || funds == null || funds < amount) return null;

            var value = amount * _currencyProvider.ListOfCurrenciesBl.SingleOrDefault(x => x.Name == currencyToSell).Value;
            
            var saleBl = new TransactionBl
            {
                Id = Guid.NewGuid(),
                Value = value,
                Type = TransactionType.Sale,
                CurrencyType = currencyToSell,
                NumberOfCoins = -amount,
                Date = DateTime.Now
            };

            var sale = _transactionMapper.MapTransactionBlToTransaction(saleBl);
            sale.User = user;
            var newTransaction = _transactionRepository.AddTransaction(sale);
            return _transactionMapper.MapTransactionToTransactionBl(newTransaction);
        }

        public List<TransactionBl> GetTransactions(int userId)
        {
            var transactions = _transactionRepository.GetTransactionsForUser(userId);
            return transactions.Select(transaction => _transactionMapper.MapTransactionToTransactionBl(transaction)).OrderBy(x => x.Date).ToList();
        }

        public AccountBalanceBl GetAccountBalance(int userId)
        {
            var user = new User
            {
                Id = userId
            };

            var accountBalance = new AccountBalanceBl
            {
                NumberOfPln = (_transactionRepository.SumAllTransactions(user) != null)? Math.Round((double) _transactionRepository.SumAllTransactions(user), 3) : 0,
                NumberOfBcc = (_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.BCC, user) != null)? Math.Round((double)_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.BCC, user), 5) : 0,
                NumberOfBtc = (_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.BTC, user) != null)? Math.Round((double)_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.BTC, user), 5) : 0,
                NumberOfEth = (_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.ETH, user) != null)? Math.Round((double)_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.ETH, user), 5) : 0,
                NumberOfLtc = (_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.LTC, user) != null)? Math.Round((double)_transactionRepository.SumTransactionsByCurrencyType(CurrencyType.LTC, user), 5) : 0,
            };

            return accountBalance;
        }

        public void ExportTransactions(string filePath, int userId)
        {
            var transactions = _transactionRepository.GetTransactionsForUser(userId).ToArray();

            _jsonFileService.Export(filePath, transactions);
        }
    }
}
