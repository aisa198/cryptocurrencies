﻿using System;
using System.Collections.Generic;
using Cryptocurrencies.BL.BusinessModels;

namespace Cryptocurrencies.BL.Services
{
    public class GeneratedEventArgs : EventArgs
    {
        public List<CurrencyBl> ListOfGeneratedCurrenciesBl;
    }
}
