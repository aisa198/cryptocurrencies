﻿using System.Collections.Generic;
using System.Linq;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Mappers;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.DAL.Repositories.Interfaces;

namespace Cryptocurrencies.BL.Services
{
    public class UserService : IUserService
    {
        private readonly IUserMapper _userMapper;
        private readonly IUserRepository _userRepository;

        public UserService(IUserMapper userMapper, IUserRepository userRepository)
        {
            _userMapper = userMapper;
            _userRepository = userRepository;
        }

        public List<UserBl> GetAllUsers()
        {
            return _userRepository.GetAllUsers().Select(x => _userMapper.MapUserToUserBl(x)).ToList();
        }

        public UserBl AddUser(UserBl userBl)
        {
            if (string.IsNullOrWhiteSpace(userBl.Login) || string.IsNullOrWhiteSpace(userBl.Password) ||
                string.IsNullOrWhiteSpace(userBl.Name) || GetAllUsers().Any(x => x.Login == userBl.Login)) return null;
            var user = _userMapper.MapUserBlToUser(userBl);
            var newUser = _userRepository.AddUser(user);
            return _userMapper.MapUserToUserBl(newUser);
        }

        public UserBl LogIn(string login)
        {
            return _userMapper.MapUserToUserBl(_userRepository.LogIn(login));
        }

        public UserAuthenticationBl CheckPassword(UserBl userBl)
        {
            var user = _userRepository.LogIn(userBl.Login);

            if (user == null) return new UserAuthenticationBl();

            var authentication = new UserAuthenticationBl
            {
                Id = user.Id,
                Name = user.Name,
                IsAuthenticated = _userRepository.CheckPassword(userBl.Login, userBl.Password)
            };

            return authentication;
        }
    }
}
