﻿using System.Collections.Generic;
using System.Net.Http;
using System.Timers;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Configuration;
using Cryptocurrencies.BL.Services.Interfaces;
using Cryptocurrencies.Comon.Enums;
using Newtonsoft.Json;

namespace Cryptocurrencies.BL.Services
{
    public class CurrencyProvider : ICurrencyProvider
    {
        private readonly IAppConfig _appConfig;
        public delegate void GeneratedHandler(object sender, GeneratedEventArgs e);
        public event GeneratedHandler RatesGenerated;
        public List<CurrencyBl> ListOfCurrenciesBl { get; }

        public CurrencyProvider(IAppConfig appConfig)
        {
            _appConfig = appConfig;
            ListOfCurrenciesBl = new List<CurrencyBl>
            {
                new CurrencyBl
                {
                    Id = 1,
                    Name = CurrencyType.BTC
                },
                new CurrencyBl
                {
                    Id = 2,
                    Name = CurrencyType.BCC
                },
                new CurrencyBl
                {
                    Id = 3,
                    Name = CurrencyType.ETH
                },
                new CurrencyBl
                {
                    Id = 4,
                    Name = CurrencyType.LTC
                }
            };

            SetValues();
            StartGenerate();
        }

        public void SetValues()
        {
            foreach (var currencyBl in ListOfCurrenciesBl)
            {
                var httpClient = new HttpClient();
                var response = httpClient.GetAsync(_appConfig.BitBayWebApiUrl + currencyBl.Name + _appConfig.PlnQuoteEndpointPostfix).Result.Content.ReadAsStringAsync();
                response.Wait();
                var deserializedResponse = JsonConvert.DeserializeObject<CurrencyFromBitbayBl>(response.Result);
                currencyBl.Value = deserializedResponse.last;
            }
        }

        public void StartGenerate()
        {
            var t = new Timer { Interval = _appConfig.RefreshingTime };
            t.Elapsed += TimerElapsedCallback;
            t.Start();
        }

        private void TimerElapsedCallback(object sender, ElapsedEventArgs e)
        {
            SetValues();
            var listOfArgs = new GeneratedEventArgs
            {
                ListOfGeneratedCurrenciesBl = ListOfCurrenciesBl
            };

            RatesGenerated?.Invoke(this, listOfArgs);
        }
    }
}
