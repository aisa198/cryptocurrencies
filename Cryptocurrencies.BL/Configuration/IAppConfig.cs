﻿namespace Cryptocurrencies.BL.Configuration
{
    public interface IAppConfig
    {
        string BitBayWebApiUrl { get; }
        string PlnQuoteEndpointPostfix { get; }
        int RefreshingTime { get; }
    }
}