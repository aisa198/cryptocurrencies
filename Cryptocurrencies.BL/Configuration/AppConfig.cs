﻿using System;
using System.Configuration;

namespace Cryptocurrencies.BL.Configuration
{
    public class AppConfig : IAppConfig
    {
        public int RefreshingTime => Convert.ToInt32(ConfigurationManager.AppSettings["RefreshingTime"]);
        public string BitBayWebApiUrl => ConfigurationManager.AppSettings["BitBayWebApiUrl"];
        public string PlnQuoteEndpointPostfix => ConfigurationManager.AppSettings["PlnQuoteEndpointPostfix"];
    }
}
