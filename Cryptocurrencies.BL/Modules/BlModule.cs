﻿using Cryptocurrencies.BL.Configuration;
using Cryptocurrencies.BL.DataExport;
using Cryptocurrencies.BL.Helpers;
using Cryptocurrencies.BL.Mappers;
using Cryptocurrencies.BL.Services;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.BL.Services.Interfaces;
using Cryptocurrencies.DAL.Modules;
using Ninject.Modules;

namespace Cryptocurrencies.BL.Modules
{
    public class BlModule : NinjectModule
    {
        public override void Load()
        {
            var kernel = Kernel;
            kernel?.Load(new[] {new DalModule()});
            Bind<IUserMapper>().To<UserMapper>();
            Bind<IUserService>().To<UserService>();
            Bind<ICurrencyGenerator>().To<CurrencyGenerator>();
            Bind<IRandomProvider>().To<RandomProvider>();
            Bind<ITransactionMapper>().To<TransactionMapper>();
            Bind<ICurrencyProvider>().To<CurrencyProvider>();
            Bind<ITransactionsService>().To<TransactionsesService>();
            Bind<IAppConfig>().To<AppConfig>();
            Bind<CurrencyProvider>().To<CurrencyProvider>().InSingletonScope();
            Bind<IJsonFileService>().To<JsonFileService>();
        }
    }
}
