import { Injectable } from '@angular/core';
import {CurrencyType, Transaction} from './Transaction';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {User} from '../security/login/User';

@Injectable()
export class TransactionsService {

  constructor(private http: HttpClient) { }

  getTransactions(userId: number): Observable<Transaction[]> {
    return this.http.get<Transaction[]>(environment.cryptocurrenciesApi + 'transactions/' + userId);
  }

  payInto(amount: number, userId: number): Observable<Transaction> {
    return this.http.get<Transaction>(environment.cryptocurrenciesApi + 'transactions/payinto?amount=' + amount + '&userId=' + userId);
  }

  withdraw(amount: number, userId: number): Observable<Transaction> {
    return this.http.get<Transaction>(environment.cryptocurrenciesApi + 'transactions/withdraw?amount=' + amount + '&userId=' + userId);
  }

  buy(amount: number, userId: number, currencyType: CurrencyType): Observable<Transaction>  {
    return this.http.get<Transaction>(environment.cryptocurrenciesApi
      + 'transactions/buy?amount=' + amount + '&userId=' + userId + '&currencyType=' + currencyType);
  }

  sell(amount: number, userId: number, currencyType: CurrencyType): Observable<Transaction>  {
    return this.http.get<Transaction>(environment.cryptocurrenciesApi
      + 'transactions/sell?amount=' + amount + '&userId=' + userId + '&currencyType=' + currencyType);
  }
}
