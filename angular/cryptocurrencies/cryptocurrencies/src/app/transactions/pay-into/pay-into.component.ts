import { Component, OnInit } from '@angular/core';
import {UserService} from '../../security/user.service';
import {TransactionsService} from '../transactions.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-pay-into',
  templateUrl: './pay-into.component.html',
  styleUrls: ['./pay-into.component.css']
})
export class PayIntoComponent implements OnInit {
  amountOfIncome: number;
  ifSuccess: boolean;

  constructor(private transactionsService: TransactionsService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.ifSuccess = true;
    this.amountOfIncome = 0;
  }

  payInto() {
    const userId = this.userService.getCurrentUser().Id;
    this.transactionsService.payInto(this.amountOfIncome, userId).subscribe(transaction => {
      if (transaction) {
        this.userService.removeAccountBalance();
        this.userService.setAccountBalance(userId);
        this.router.navigateByUrl('/transactions');
      } else {
        this.ifSuccess = false;
      }
    }, error => this.ifSuccess = false);
  }
}
