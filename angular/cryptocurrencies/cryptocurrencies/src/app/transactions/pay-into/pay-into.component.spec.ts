import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PayIntoComponent } from './pay-into.component';

describe('PayIntoComponent', () => {
  let component: PayIntoComponent;
  let fixture: ComponentFixture<PayIntoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PayIntoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayIntoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
