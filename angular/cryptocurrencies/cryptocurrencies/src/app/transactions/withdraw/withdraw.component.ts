import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {TransactionsService} from '../transactions.service';
import {UserService} from '../../security/user.service';

@Component({
  selector: 'app-withdraw',
  templateUrl: './withdraw.component.html',
  styleUrls: ['./withdraw.component.css']
})
export class WithdrawComponent implements OnInit {
  amountOfWithdraw: number;
  ifSuccess: boolean;

  constructor(private transactionsService: TransactionsService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.ifSuccess = true;
    this.amountOfWithdraw = 0;
  }

  withdraw() {
    const userId = this.userService.getCurrentUser().Id;
    this.transactionsService.withdraw(this.amountOfWithdraw, userId).subscribe(transaction => {
      if (transaction) {
        this.userService.removeAccountBalance();
        this.userService.setAccountBalance(userId);
        this.router.navigateByUrl('/transactions');
      } else {
        this.ifSuccess = false;
      }
    });
  }
}
