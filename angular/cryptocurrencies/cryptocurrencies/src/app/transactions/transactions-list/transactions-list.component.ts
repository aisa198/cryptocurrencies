import {Component, OnInit} from '@angular/core';
import {CurrencyType, Transaction, TransactionType} from '../Transaction';
import {TransactionsService} from '../transactions.service';
import {UserService} from '../../security/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-transactions-list',
  templateUrl: './transactions-list.component.html',
  styleUrls: ['./transactions-list.component.css']
})
export class TransactionsListComponent implements OnInit {

  transactions: Transaction[] = [];
  userId: number;
  userName: string;
  TransactionType: typeof TransactionType = TransactionType;
  CurrencyType: typeof CurrencyType = CurrencyType;

  constructor(private transactionsService: TransactionsService,
              private userService: UserService,
              private router: Router) {
  }



  makePositive(value : number){
    return Math.abs(value);
  }

  ngOnInit() {
    if (this.userService.getCurrentUser()) {
      this.userId = this.userService.getCurrentUser().Id;
      this.userName = this.userService.getCurrentUser().Name;
      this.transactionsService.getTransactions(this.userId)
        .subscribe(data => this.transactions = data);
    } else {
      this.router.navigateByUrl('');
    }
  }
}
