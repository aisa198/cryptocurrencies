import {User} from '../security/login/User';

export enum TransactionType {
  Income,
  Withdraw,
  Purchase,
  Sale
}

export enum CurrencyType {
  BTC,
  BCC,
  ETH,
  LTC
}

export class Transaction {
  Id: string;
  UserBl: User;
  Date: Date;
  CurrencyType: CurrencyType;
  Type: TransactionType;
  NumberOfCoins: number;
  Value: number;
}
