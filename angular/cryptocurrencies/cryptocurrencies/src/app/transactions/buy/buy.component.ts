import { Component, OnInit } from '@angular/core';
import {CurrencyType, Transaction} from '../Transaction';
import {UserService} from '../../security/user.service';
import {Router} from '@angular/router';
import {TransactionsService} from '../transactions.service';

@Component({
  selector: 'app-buy',
  templateUrl: './buy.component.html',
  styleUrls: ['./buy.component.css']
})
export class BuyComponent implements OnInit {
  amountToExchange: number;
  currencyType: CurrencyType;
  currencyTypes: string[] = Object.keys(CurrencyType);
  ifSuccess: boolean;

  constructor(private transactionsService: TransactionsService,
              private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.currencyTypes = this.currencyTypes.slice(this.currencyTypes.length / 2);
    this.ifSuccess = true;
    this.amountToExchange = 0;
  }

  buy() {
    const userId = this.userService.getCurrentUser().Id;
    this.transactionsService.buy(this.amountToExchange, userId, this.currencyType).subscribe(transaction => {
      if (transaction) {
        this.userService.removeAccountBalance();
        this.userService.setAccountBalance(userId);
        this.router.navigateByUrl('/transactions');
      } else {
        this.ifSuccess = false;
      }
    }, error => this.ifSuccess = false);
  }
}
