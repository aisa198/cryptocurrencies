import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TransactionsListComponent } from './transactions/transactions-list/transactions-list.component';
import { LoginComponent } from './security/login/login.component';
import {TransactionsService} from './transactions/transactions.service';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AuthenticationService} from './security/authentication/authentication.service';
import {UserService} from './security/user.service';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './navbar/navbar.component';
import { RegisterComponent } from './security/register/register.component';
import { PayIntoComponent } from './transactions/pay-into/pay-into.component';
import { WithdrawComponent } from './transactions/withdraw/withdraw.component';
import { BuyComponent } from './transactions/buy/buy.component';
import { SellComponent } from './transactions/sell/sell.component';


@NgModule({
  declarations: [
    AppComponent,
    TransactionsListComponent,
    LoginComponent,
    NavbarComponent,
    RegisterComponent,
    PayIntoComponent,
    WithdrawComponent,
    BuyComponent,
    SellComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [
    TransactionsService,
    AuthenticationService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
