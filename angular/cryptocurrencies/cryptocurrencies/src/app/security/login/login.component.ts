import {Component, OnInit} from '@angular/core';
import {Credentials} from '../Credentials';
import {AuthenticationService} from '../authentication/authentication.service';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userCredentials: Credentials = new Credentials();
  authentication = true;

  constructor(private authenticationService: AuthenticationService,
              private userService: UserService,
              private router: Router) {
  }

  ngOnInit() {
    if (this.userService.getCurrentUser()) {
      this.router.navigateByUrl('/transactions');
    }
  }

  login() {
    this.authenticationService.login(this.userCredentials)
      .subscribe(user => {
        if (user.IsAuthenticated) {
          this.userService.setCurrentUser(user);
          this.userService.setAccountBalance(user.Id);
          this.router.navigateByUrl('/transactions');
        } else {
          this.authentication = false;
        }
        this.userCredentials = new Credentials();
      });
  }
}
