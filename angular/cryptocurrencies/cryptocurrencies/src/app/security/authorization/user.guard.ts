import { Injectable } from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild, Router} from '@angular/router';
import { Observable } from 'rxjs/Observable';
import {UserService} from '../user.service';

@Injectable()
export class UserGuard implements CanActivate, CanActivateChild {

  constructor(private userService: UserService,
              private router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): boolean {
    const currentUser = this.userService.getCurrentUser();
    if (currentUser && currentUser.IsAuthenticated) {
      return true;
    }
    this.router.navigateByUrl('/login');
    return false;
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot,
                   state: RouterStateSnapshot): boolean {
    return this.canActivate(childRoute, state);
  }
}
