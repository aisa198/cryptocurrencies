import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {UserService} from '../user.service';
import {Credentials} from '../Credentials';
import {Observable} from 'rxjs/Observable';
import {User} from '../login/User';
import {environment} from '../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient,
              private userService: UserService) { }

  login(credentials: Credentials): Observable<User> {
    return this.http
      .post<any>(environment.cryptocurrenciesApi + 'user', credentials)
      .pipe(
        map(response => {
          return new User(response.Id, response.Name, response.IsAuthenticated);
        })
      );
  }

  logout()  {
    this.userService.removeUser();
  }

}
