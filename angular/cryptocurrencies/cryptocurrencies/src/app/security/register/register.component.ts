import { Component, OnInit } from '@angular/core';
import {RegisterUser} from './RegisterUser';
import {UserService} from '../user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user: RegisterUser;
  ifSuccess: boolean;

  constructor(private userService: UserService,
              private router: Router) { }

  ngOnInit() {
    this.user = new RegisterUser();
    this.ifSuccess = true;
  }

  register() {
    this.userService.registerUser(this.user)
      .subscribe(user => {
        if (user) {
          this.router.navigateByUrl('/login');
        } else {
          this.ifSuccess = false;
        }
        this.user = new RegisterUser();
      });
  }

}
