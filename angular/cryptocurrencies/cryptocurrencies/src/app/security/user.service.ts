import { Injectable } from '@angular/core';
import {User} from './login/User';
import {RegisterUser} from './register/RegisterUser';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/Observable';
import {HttpClient} from '@angular/common/http';
import {AccountBalance} from './AccountBalance';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) { }

  setCurrentUser(user: User) {
    const userJSON = JSON.stringify(user);
    localStorage.setItem('user', userJSON);
  }

  getCurrentUser(): User {
    const userJSON = localStorage.getItem('user');
    if (userJSON) {
      return JSON.parse(userJSON) as User;
    }
    return null;
  }

  removeUser() {
    localStorage.removeItem('user');
  }

  registerUser(registerUser: RegisterUser): Observable<RegisterUser> {
    return this.http.post<RegisterUser>(environment.cryptocurrenciesApi + 'user/register', registerUser);
  }

  setAccountBalance(userId: number) {
    let accountBalance: AccountBalance;
    this.http.get<AccountBalance>(environment.cryptocurrenciesApi + 'transactions/account?id=' +  userId)
      .subscribe(data => {
      accountBalance = data;
        const accountBalanceJSON = JSON.stringify(accountBalance);
        localStorage.setItem('account', accountBalanceJSON);
    });
  }

  getAccountBalance(): AccountBalance {
    const accountBalanceJSON = localStorage.getItem('account');
    if (accountBalanceJSON) {
      return JSON.parse(accountBalanceJSON) as AccountBalance;
    }
    return null;
  }

  removeAccountBalance() {
    localStorage.removeItem('account');
  }
}
