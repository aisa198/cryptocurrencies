import { Component, OnInit } from '@angular/core';
import {UserService} from '../security/user.service';
import {Router} from '@angular/router';
import {AccountBalance} from '../security/AccountBalance';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(public userService: UserService,
              private router: Router) { }

  ngOnInit() {
  }

  logOut() {
    this.userService.removeUser();
    this.router.navigateByUrl('/login');
    this.userService.removeAccountBalance();
  }
}
