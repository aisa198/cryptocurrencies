import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './security/login/login.component';
import {TransactionsListComponent} from './transactions/transactions-list/transactions-list.component';
import {RegisterComponent} from './security/register/register.component';
import {PayIntoComponent} from './transactions/pay-into/pay-into.component';
import {WithdrawComponent} from './transactions/withdraw/withdraw.component';
import {BuyComponent} from './transactions/buy/buy.component';
import {SellComponent} from './transactions/sell/sell.component';
import {UserGuard} from './security/authorization/user.guard';

const routes: Routes = [
  {path: '', component: LoginComponent },
  {path: 'login', component: LoginComponent },
  {path: 'register', component: RegisterComponent },
  {path: 'transactions', canActivate: [UserGuard], component: TransactionsListComponent},
  {path: 'payinto', canActivate: [UserGuard], component: PayIntoComponent},
  {path: 'withdraw', canActivate: [UserGuard], component: WithdrawComponent},
  {path: 'buy', canActivate: [UserGuard], component: BuyComponent},
  {path: 'sell', canActivate: [UserGuard], component: SellComponent},
  {path: '**', component: LoginComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ],
  providers: [UserGuard],
})
export class AppRoutingModule {}
