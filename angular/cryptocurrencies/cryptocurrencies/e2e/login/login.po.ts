import { browser, by, element } from 'protractor';

export class LoginPage {
  navigateTo() {
    return browser.get('/');
  }

  setLogin(login: string)  {
    element(by.id('user')).sendKeys(login);
  }

  setPassword(password: string)  {
    element(by.id('password')).sendKeys(password);
  }

  login()  {
    element(by.id('submitButton')).click();
  }

  isLoginContainerPresent()  {
    return element(by.id('loginContainer')).isPresent();
  }
}
