import {LoginPage} from './login.po';

describe('Login page', () => {
  let page: LoginPage;

  beforeEach(() => {
    page = new LoginPage();
  });

  it('should login test user', () => {
    page.navigateTo();
    page.setLogin('a');
    page.setPassword('a');
    page.login();
    expect(page.isLoginContainerPresent()).toBe(false);
  });
});
