import { browser, by, element } from 'protractor';

export class Navbar {
  navigateTo() {
    return browser.get('/transactions');
  }

  payinto()  {
    element(by.id('payinto')).click();
  }

  withdraw()  {
    element(by.id('withdraw')).click();
  }

  buy()  {
    element(by.id('buy')).click();
  }

  sell()  {
    element(by.id('sell')).click();
  }

  logout()  {
    element(by.id('logout')).click();
  }

  register()  {
    element(by.id('register')).click();
  }

  isAccountBallanceContainerPresent()  {
    return element(by.id('accountBallanceContainer')).isPresent();
  }

  isLoginContainerPresent()  {
    return element(by.id('loginContainer')).isPresent();
  }

  isPayContainerPresent()  {
    return element(by.id('payContainer')).isPresent();
  }

  isWithdrawContainerPresent()  {
    return element(by.id('withdrawContainer')).isPresent();
  }

  isSellContainerPresent()  {
    return element(by.id('sellContainer')).isPresent();
  }

  isBuyContainerPresent()  {
    return element(by.id('buyContainer')).isPresent();
  }

  isRegisterContainerPresent()  {
    return element(by.id('registerContainer')).isPresent();
  }

}
