import {Navbar} from './navbar.po';

describe('Navbar', () => {
  let page: Navbar;

  beforeEach(() => {
    page = new Navbar();
  });

  it('should show account balance', () => {
    expect(page.isAccountBallanceContainerPresent()).toBe(true);
  });
  it('should redirect to pay-into component', () => {
    page.navigateTo();
    page.payinto();
    expect(page.isPayContainerPresent()).toBe(true);
  });
  it('should redirect to withdraw component', () => {
    page.navigateTo();
    page.withdraw();
    expect(page.isWithdrawContainerPresent()).toBe(true);
  });
  it('should redirect to buy component', () => {
    page.navigateTo();
    page.buy();
    expect(page.isBuyContainerPresent()).toBe(true);
  });
  it('should redirect to sell component', () => {
    page.navigateTo();
    page.sell();
    expect(page.isSellContainerPresent()).toBe(true);
  });
  it('should logout user', () => {
    page.navigateTo();
    page.logout();
    expect(page.isLoginContainerPresent()).toBe(true);
  });
  it('should redirect to register component', () => {
    page.register();
    expect(page.isRegisterContainerPresent()).toBe(true);
  });
});
