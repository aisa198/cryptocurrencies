﻿using System;
using System.Net.Http;
using Microsoft.Owin.Hosting;

namespace Cryptocurrencies.WebApii
{
    internal class Program
    {
        private static void Main()
        {
            const string apiAdress = "http://localhost:9000";

            WebApp.Start<StartUp>(apiAdress); 

            var httpClient = new HttpClient();
            var response = httpClient.GetAsync(apiAdress + "/api/status").Result;

            Console.WriteLine($"Status: {response.Content.ReadAsStringAsync().Result} Status code: {response.StatusCode}");

            Console.ReadKey();
        }
    }
}
