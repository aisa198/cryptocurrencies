﻿using Cryptocurrencies.BL.Modules;
using Ninject;

namespace Cryptocurrencies.WebApii.Bootstrap
{
    internal class NinjectBootstrap
    {
        public static IKernel GetKernel()
        {
            var kernel = new StandardKernel();
            kernel.Load(new[] { new BlModule()});
            return kernel;
        }
    }
}
