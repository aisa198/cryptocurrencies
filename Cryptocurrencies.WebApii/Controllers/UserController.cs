﻿using System.Web.Http;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Services.interfaces;

namespace Cryptocurrencies.WebApii.Controllers
{
    public class UserController : ApiController
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPost]
        public UserAuthenticationBl CheckAuthentication (UserBl userBl)
        {
            return _userService.CheckPassword(userBl);
        }

        [HttpPost]
        [Route("api/user/register")]
        public UserBl AddUser(UserBl userBl)
        {
            return _userService.AddUser(userBl);
        }
    }
}
