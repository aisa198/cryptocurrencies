﻿using System.Collections.Generic;
using System.Web.Http;
using Cryptocurrencies.BL.BusinessModels;
using Cryptocurrencies.BL.Services.interfaces;
using Cryptocurrencies.Comon.Enums;

namespace Cryptocurrencies.WebApii.Controllers
{
    public class TransactionsController : ApiController
    {
        private readonly ITransactionsService _transactionsService;

        public TransactionsController(ITransactionsService transactionsService)
        {
            _transactionsService = transactionsService;
        }
        
        public IEnumerable<TransactionBl> GetTransactionsForUSer(int id)
        {
            return _transactionsService.GetTransactions(id);
        }

        [Route("api/transactions/account")]
        public AccountBalanceBl GetAccountBalance(int id)
        {
            return _transactionsService.GetAccountBalance(id);
        }

        [HttpGet]
        [Route("api/transactions/payinto")]
        public TransactionBl RegisterIncome(double amount, int userId)
        {
            return _transactionsService.RegisterIncome(amount, userId);
        }

        [HttpGet]
        [Route("api/transactions/withdraw")]
        public TransactionBl RegisterWithdraw(double amount, int userId)
        {
            return _transactionsService.RegisterWithdraw(amount, userId);
        }

        [HttpGet]
        [Route("api/transactions/buy")]
        public TransactionBl RegisterPurchase(double amount, int userId, CurrencyType currencyType)
        {
            return _transactionsService.RegisterPurchase(amount, userId, currencyType);
        }

        [HttpGet]
        [Route("api/transactions/sell")]
        public TransactionBl RegisterSale(double amount, int userId, CurrencyType currencyType)
        {
            return _transactionsService.RegisterSale(amount, userId, currencyType);
        }
    }
}

