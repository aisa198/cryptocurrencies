﻿using System.Web.Http;

namespace Cryptocurrencies.WebApii.Controllers
{
    public class StatusController : ApiController
    {
        public string Get()
        {
            return "ok";
        }
    }
}
